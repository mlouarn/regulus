#!/usr/bin/env sh

source /local/env/envsnakemake-5.20.1.sh

PIPELINE=__PATH__/scripts/pipeline.smk
CONFIG_FILE=__CONFIG_FILE__
PROFILE_DIR=__PROFILE_DIR__

snakemake -s $PIPELINE --configfile $CONFIG_FILE --profile $PROFILE_DIR --unlock
snakemake -s $PIPELINE --configfile $CONFIG_FILE --profile $PROFILE_DIR
