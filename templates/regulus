#!/bin/bash

################################################################################
### START OF CODE GENERATED BY Argbash v2.8.1 one line above
################################################################################
# Argbash is a bash code generator used to get arguments parsing right.
# Argbash is FREE SOFTWARE, see https://argbash.io for more info
# Generated online by https://argbash.io/generate
die()
{
	local _ret=$2
	test -n "$_ret" || _ret=1
	test "$_PRINT_HELP" = yes && print_help >&2
	echo "$1" >&2
	exit ${_ret}
}

begins_with_short_option()
{
	local first_option all_short_options='ijonchv'
	first_option="${1:0:1}"
	test "$all_short_options" = "${all_short_options/$first_option/}" && return 1 || return 0
}

# THE DEFAULTS INITIALIZATION - OPTIONALS
_arg_input=
_arg_output=
_arg_cluster=
_arg_jobs="4"
_arg_dry_run="off"

print_help()
{
	printf '%s\n' "Regulus, a pipeline for regulatory circuits analysis"
	printf 'Usage: %s -i|--input <arg> -o|--output <arg> [-c|--cluster <arg>] [-h|--help]\n' "$0"
	printf '\t%s\n' "-i, --input: Input directory absolute path (mandatory)"
	printf '\t%s\n' "-o, --output: Output directory absolute path (mandatory)"
	printf '\t%s\n' "-c, --cluster: Command to launch jobs on your cluster (sbatch, qsub...)"
	printf '\t%s\n' "-n, --dry-run: dry-run of regulus (off by default)"
  	printf '\t%s\n' "-j, --jobs: Number of parallel jobs (default: '4')"
	printf '\t%s\n' "-h, --help: Prints help"
  	printf '\t%s\n' "-v, --version: Prints version"
}

parse_commandline()
{
	while test $# -gt 0
	do
		_key="$1"
		case "$_key" in
			-i|--input)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_input="$2"
				shift
				;;
			--input=*)
				_arg_input="${_key##--input=}"
				;;
			-i*)
				_arg_input="${_key##-i}"
				;;
      		-j|--jobs)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_jobs="$2"
				shift
				;;
			--jobs=*)
				_arg_jobs="${_key##--jobs=}"
				;;
			-j*)
				_arg_jobs="${_key##-j}"
				;;
			-o|--output)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_output="$2"
				shift
				;;
			--output=*)
				_arg_output="${_key##--output=}"
				;;
			-o*)
				_arg_output="${_key##-o}"
				;;
			-n|--no-dry-run|--dry-run)
				_arg_dry_run="on"
				test "${1:0:5}" = "--no-" && _arg_dry_run="off"
				;;
			-n*)
				_arg_dry_run="on"
				_next="${_key##-n}"
				if test -n "$_next" -a "$_next" != "$_key"
				then
					{ begins_with_short_option "$_next" && shift && set -- "-n" "-${_next}" "$@"; } || die "The short option '$_key' can't be decomposed to ${_key:0:2} and -${_key:2}, because ${_key:0:2} doesn't accept value and '-${_key:2:1}' doesn't correspond to a short option."
				fi
				;;
			-c|--cluster)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_cluster="$2"
				shift
				;;
			--cluster=*)
				_arg_cluster="${_key##--cluster=}"
				;;
			-c*)
				_arg_cluster="${_key##-c}"
				;;
			-h|--help)
				print_help
				exit 0
				;;
			-h*)
				print_help
				exit 0
				;;
      		-v|--version)
				printf '%s %s\n\n%s\n' "Regulus" "0.1" 'Regulus, a pipeline for regulatory circuits'
				exit 0
				;;
			-v*)
				printf '%s %s\n\n%s\n' "Regulus" "0.1" 'Regulus, a pipeline for regulatory circuits'
				exit 0
				;;
			*)
				_PRINT_HELP=yes die "FATAL ERROR: Got an unexpected argument '$1'" 1
				;;
		esac
		shift
	done
}
parse_commandline "$@"
################################################################################
### END OF CODE GENERATED BY Argbash
################################################################################

if [ -z $_arg_input ]
then
  _PRINT_HELP=yes die "Missing value for input argument (mandatory)." 1
fi

if [ -z $_arg_output ]
then
  _PRINT_HELP=yes die "Missing value for output argument (mandatory)." 1
fi

if [ ! "${_arg_input:0:1}" = "/" ]
then
  _PRINT_HELP=yes die "FATAL ERROR: absolute path needed for input directory" 1
fi

if [ ! "${_arg_output:0:1}" = "/" ]
then
  _PRINT_HELP=yes die "FATAL ERROR: absolute path needed for output directory" 1
fi

PIPELINE_PATH="__PIPELINE_PATH__"
CONDA_DIR=$PIPELINE_PATH/conda_env
PROFILE_DIR=$_arg_output/workdir/smk_profile
PROFILE_FILE=$PROFILE_DIR/config.yaml
CONFIG_FILE=$_arg_output/workdir/smk_config.yaml
JOB_FILE=$_arg_output/workdir/regulus_job.sh

################################################################################
### PREPARE OUTPUT DIRECTORY AND FILES
################################################################################


### Output directory
echo -e "\nCreate output work directory"
echo -e "-> $_arg_output/workdir"


### Snakemake profile
echo -e "\nCreate snakemake profile file"
echo -e "-> $PROFILE_FILE"
mkdir -p $PROFILE_DIR
CLUSTER_LINE=""
if [ -n "$_arg_cluster" ]
then
  CLUSTER_LINE="cluster: $_arg_cluster"
fi
sed "s|__JOBS__|jobs: $_arg_jobs|g" ./templates/smk_profile.yaml | \
sed "s|__CLUSTER__|$CLUSTER_LINE|g" | \
sed "s|__PATH__|$PIPELINE_PATH/conda_env|g" \
> $PROFILE_FILE

### Snakemake config
echo -e "\nCreate snakemake config file"
echo -e "-> $CONFIG_FILE"
sed "s|__PATH__|$PIPELINE_PATH|g" ./templates/smk_config.yaml | \
sed "s|__OUTPUT_DIR__|$_arg_output|g" | \
sed "s|__INPUT_DIR__|$_arg_input|g" \
> $CONFIG_FILE

### Regulus job.sh
echo -e "\nCreate job file"
echo -e "-> $JOB_FILE"
sed "s|__PATH__|$PIPELINE_PATH|g" ./templates/job.sh | \
sed "s|__CONFIG_FILE__|$CONFIG_FILE|g" | \
sed "s|__PROFILE_DIR__|$PROFILE_DIR|g" \
> $JOB_FILE

# Launch job with cluster or not
if [ $_arg_dry_run = "off" ]
then
	if [ -n "$_arg_cluster" ]
	then
		echo -e "Ready to launch: $_arg_cluster $JOB_FILE"
		read -p "is it ok? [y|N]: " -n 1 -r
		echo
		if [[ $REPLY =~ ^[Yy]$ ]]
		then
			$_arg_cluster $JOB_FILE
		fi
	else
	echo -e "Ready to launch: sh $JOB_FILE"
		read -p "is it ok? [y|N]: " -n 1 -r
		echo
		if [[ $REPLY =~ ^[Yy]$ ]]
		then
			sh $JOB_FILE
		fi
	
	fi
fi