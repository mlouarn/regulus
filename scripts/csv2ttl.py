#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Quick and dirty converter of CSV files into TTL.

Usage: csv2ttl.py -i <file.csv> -o <file.ttl>

dc:creator Olivier Dameron
dc:creator https://orcid.org/0000-0001-8959-7189
"""

import argparse
import csv
import sys

def isfloat(value):
	try:
		float(value)
		return True
	except ValueError:
		return False

def isint(value):
	try:
		int(value)
		return True
	except ValueError:
		return False

def generateURI(entityIdent):
	colonPos = entityIdent.find(':')
	if entityIdent.startswith("http"):
		entityURI = "<" + entityIdent + ">"
	elif colonPos != -1:
		(entityPrefix,entityLocalIdent) = entityIdent.split(':', maxsplit=1)
		if entityPrefix not in knownPrefixes.keys():
			if entityPrefix not in unknownPrefixes:
				unknownPrefixes.append(entityPrefix)
		entityURI = entityIdent
	else:
		entityURI = ":" + entityIdent
	return entityURI

# Read command line and set args
parser = argparse.ArgumentParser(prog='tsv2ttl', description='Quick and dirty converter of CSV files into TTL')
parser.add_argument('-i', '--input_file',  nargs='?', help='Input file (TSV)')
parser.add_argument('-o', '--output_file', nargs='?', help='Output file (TTL)')
args = parser.parse_args()

# If an argument is missing -> usage and exit
if not args.input_file or not args.output_file:
    parser.print_help()
    exit(1)

# Get file names
tsvFileName = args.input_file
ttlFileName = args.output_file

# Define prefixes
knownPrefixes = {}
knownPrefixes['rdf'] = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'
knownPrefixes['rdfs'] = 'http://www.w3.org/2000/01/rdf-schema#'
knownPrefixes['owl'] = 'http://www.w3.org/2002/07/owl#'
knownPrefixes['xsd'] = 'http://www.w3.org/2001/XMLSchema#'
knownPrefixes['skos'] = 'http://www.w3.org/2004/02/skos/core#'
knownPrefixes['default'] = 'http://askomics.org/data/'

unknownPrefixes = []

prefixesTTL = ""
for (ns, uri) in knownPrefixes.items():
	if ns == "default":
		continue
	prefixesTTL += "@prefix {}: <{}> .\n".format(ns, uri)
prefixesTTL += "\n"
prefixesTTL += "@prefix : <{}> .\n".format(knownPrefixes["default"])
prefixesTTL += "\n\n"

# Convert TSV in CSV
with open(tsvFileName) as csvFile:
	with open(ttlFileName, 'w') as rdfFile:
		rdfFile.write(prefixesTTL)
		datareader = csv.reader(csvFile, delimiter=",")
		headers = next(datareader)
		for row in datareader:
			# handles blank lines gracefully
			if row == []:
				continue
			nbCol = len(row)
			refEntity = generateURI(row[0])

			rdfFile.write("{} rdf:type {} .\n".format(refEntity, generateURI(headers[0])))
			for numCol in range(1, nbCol):
				sepIndex = headers[numCol].find("@")
				if sepIndex == -1:
					relationName = generateURI(headers[numCol])
					if isint(row[numCol]):
						rdfFile.write("{} {} \"{}\"^^xsd:int .\n".format(refEntity, relationName, row[numCol]))
					elif isfloat(row[numCol]):
						rdfFile.write("{} {} \"{}\"^^xsd:float .\n".format(refEntity, relationName, row[numCol]))
					else:
						rdfFile.write("{} {} \"{}\"^^xsd:string .\n".format(refEntity, relationName, row[numCol]))
				else:
					relationName = generateURI(headers[numCol][:sepIndex])
					objectClass = generateURI(headers[numCol][sepIndex+1:])
					rdfFile.write("{} {} {} .\n".format(refEntity, relationName, generateURI(row[numCol])))
					rdfFile.write("{} rdf:type {} .\n".format(generateURI(row[numCol]), objectClass))
			rdfFile.write("\n")
		if len(unknownPrefixes) > 0:
			print("\nWARNING: the following prefixes were observed in the data from {} but are unknown. Random values were generated for them at the end of the RDF file. Fix the data or consider declaring the prefixes in the {} script".format(sys.argv[1], sys.argv[0]))
			rdfFile.write("\n")
			rdfFile.write("# UNKNOWN PREFIXES found in the spreadsheet {}\n".format(sys.argv[1]))
			for prefix in unknownPrefixes:
				print("UNKNOWN PREFIX: " + prefix)
				rdfFile.write("@prefix {}: <{}{}/> .\n".format(prefix, knownPrefixes["default"], prefix))
