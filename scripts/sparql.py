from SPARQLWrapper import SPARQLWrapper, JSON
import time

sparql = SPARQLWrapper("http://localhost:8890/sparql")

sparql.setQuery("""
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX : <http://askomics.org/data/> 
    SELECT DISTINCT ?gene
	WHERE {
        ?gene rdf:type :Gene .
    }
""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

genes_list = [result["gene"]["value"].split(sep="/")[-1] for result in results["results"]["bindings"]]
print("Gene", "Gene_Pattern", "Region", "Region_Pattern", "TF", "TF_Pattern", sep='\t')
for genes in genes_list:
    uri = ("<http://askomics.org/data/" + genes + ">")
    start = time.time()
    sparql.setQuery("""
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX : <http://askomics.org/data/> 
        SELECT DISTINCT ?gene ?genePattern ?region ?regionPattern ?tf ?tfPattern ?neighborhoodDist
        WHERE {
            ?gene :Gene_Pattern ?genePattern .
            ?gene rdf:type :Gene .

            ?neighborhood :nextto ?gene .
            ?neighborhood rdf:type :id_Region_close .
            ?neighborhood :nextto ?region .
            ?region rdf:type :Region .
            ?neighborhood :Distance ?neighborhoodDist .
            ?region :Region_Pattern ?regionPattern .

            ?TFregionInclusion :has_binding_site_in ?region .
            ?TFregionInclusion rdf:type :TF_inclusion_Region .
            ?TFregionInclusion :binding_site_of_TF ?tf .
            ?tf rdf:type :TF .
            #OPTIONAL {
            ?tf :TF_Pattern ?tfPattern .
            #}
            VALUES ?gene { """ + uri + """} .
        }
    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    print("time = ", time.time() - start, "for", len(results["results"]["bindings"]), "rows")
    for result in results["results"]["bindings"]:
        gene = result["gene"]["value"].split(sep="/")[-1]
        region = result["region"]["value"].split(sep="/")[-1]
        tf = result["tf"]["value"].split(sep="/")[-1]
        dist = result["neighborhoodDist"]["value"].split(sep="/")[-1]
        genePattern = result["genePattern"]["value"].split(sep="/")[-1]
        regionPattern = result["regionPattern"]["value"].split(sep="/")[-1]
        tfPattern = result["tfPattern"]["value"].split(sep="/")[-1]
        print(gene, genePattern, region, regionPattern, tf, tfPattern, sep='\t')
