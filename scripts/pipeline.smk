RDF_INPUT = ["tf_region_intersect_filtered","gene_region_distance_filtered", "gene_pattern","region_pattern","tf_pattern"]

input_dir = config["INPUT_DIR"]
output_dir = config["OUTPUT_DIR"]
log_dir = config["LOG_DIR"]
pipeline_path = config["PIPELINE_PATH"]

rule all:
    input:
#        output_dir + "/result.txt",
        output_dir + "/gene_pattern_count.csv",
#        output_dir + "/TF_Gene_allProfiles.csv"
        output_dir + "/TF_Gene_ConsistRel.csv"

# (STEP1)
# Rule gene_exp_patterns
#  -> Generate patterns for gene expression
#
rule gene_exp_patterns:
    input:
        input_dir + "/gene_expression.csv"
    output:
        output_dir + "/gene_pattern.csv"
    log:
        log_dir + "/gene_exp_patterns.log"
    message:
        "Calculate gene expression patterns"
    conda:
        pipeline_path + "/conda_env/python3.8.yaml"
    shell:
        pipeline_path + "/scripts/gene2pattern.py -g {input} -o {output}"

# (STEP1.1)
# Rule gene_pattern_count
#  -> Count the number of genes per pattern
#
rule gene_pattern_counts:
    input:
        output_dir + "/gene_pattern.csv"
    output:
        output_dir + "/gene_pattern_count.csv"
    log:
        log_dir + "/gene_pattern_count.log"
    message:
        "Count the number of genes per pattern"
    conda:
        pipeline_path + "/conda_env/python3.8.yaml"
    shell:
        pipeline_path + "/scripts/gene_pattern_count.py -g {input} -o {output} 2> {log}"

# (STEP1)
# Rule tf_exp_patterns
#  -> Generate patterns for gene expression
#
rule tf_exp_patterns:
    input:
        gene_exp = output_dir + "/gene_pattern.csv",
        tf_list = "ref/valid_tf.csv",
    output:
        output_dir + "/tf_pattern.csv"
    log:
        log_dir + "/tf_exp_patterns.log"
    message:
        "Calculate tf expression patterns"
    conda:
        pipeline_path + "/conda_env/python3.8.yaml"
    shell:
        pipeline_path + "/scripts/gene2tf.py -g {input.gene_exp} -t {input.tf_list} -o {output} 2> {log}"

# (STEP1)
# Rule region_read_density_patterns
#  -> Generate patterns for read density of regions
#
rule region_read_density_patterns:
    input:
        input_dir + "/region_read_density.csv"
    output:
        output_dir + "/region_pattern.csv"
    log:
        log_dir + "/region_read_density_patterns.log"
    message:
        "Calculate region read-density patterns"
    conda:
        pipeline_path + "/conda_env/python3.8.yaml"
    shell:
        pipeline_path + "/scripts/region2pattern.py -r {input} -o {output}"

# (STEP2)
# Rule gene_region_distance
#  -> Calculate the distances between genes and regions
#
rule gene_region_distance:
    input:
        gene_file = input_dir + "/gene_localisation.bed",
        region_file = input_dir + "/region_localisation.bed"
    output:
        output_dir + "/gene_region_distance.csv"
    log:
        log_dir + "/gene_region_distance.log"
    message:
        "Calculate gene-region distances"
    conda:
        pipeline_path + "/conda_env/python3.8.yaml"
    shell:
        pipeline_path + "/scripts/distance.py -g {input.gene_file} -r {input.region_file} -o {output} 2> {log}"

# (STEP4)
# Rule gene_region_filter
#  -> Filter the distances between genes and regions
#
rule gene_region_filter:
    input:
        output_dir + "/gene_region_distance.csv"
    output:
        output_dir + "/gene_region_distance_filtered.csv"
    log:
        log_dir + "/gene_region_filter.log"
    message:
        "Filter gene-region distances"
    conda:
        pipeline_path + "/conda_env/python3.8.yaml"
    shell:
        pipeline_path + "/scripts/filter_dist.py -g {input} -o {output}  2> {log}"

# (STEP3)
# Rule tf_region_intersect
#  -> Calculate the intersection between transcription factors and regions
#
SUB=['1','2','3','4']
rule tf_region_intersect:
    input:
        tf_loc = pipeline_path + "/ref/tf_loc_sub{sub}.bed",
        reg_loc = input_dir + "/region_localisation.bed",
        gref = pipeline_path + "/ref/hg19.genome2"
    output:
        temp(output_dir + "/tf_region_intersect_sub{sub}.csv")
    log:
        log_dir + "/tf_region_intersect_sub{sub}.log"
    message:
        "Calculate TF-region intersection"
    conda:
        pipeline_path + "/conda_env/bedtools.yaml"
    shell:
        "bedtools intersect -F 1 -wa -wb -a {input.reg_loc} -b {input.tf_loc} -sorted -g {input.gref} > {output} 2> {log}"

rule tf_region_combine:
    input:
        expand(output_dir + "/tf_region_intersect_sub{sub}.csv", sub=SUB)
    output:
        output_dir + "/tf_region_intersect.csv"
    log:
        log_dir + "/tf_region_intersect.log"
    message:
        "Combine TF-region intersection"
    shell:
        "cat {input} > {output} 2> {log}"

# (STEP4)
# Rule tf_region_filter
#  -> Filter the intersection between transcription factors and regions
#
rule tf_region_filter:
    input:
        intersect = output_dir + "/tf_region_intersect.csv"
    output:
        output_dir + "/tf_region_intersect_filtered.csv"
    log:
        log_dir + "/tf_region_filter.log"
    message:
        "Filter TF-region intersection"
    conda:
        pipeline_path + "/conda_env/python3.8.yaml"
    shell:
        pipeline_path + "/scripts/filter_inter.py -i {input.intersect} -o {output} 2> {log}"

# (STEP5)
# Rule rdf_transform
#  -> Transform all input files in RDF format file
#
rule rdf_transform:
    input:
        output_dir + "/{file}.csv"
    output:
        output_dir + "/{file}.ttl"
    log:
        log_dir + "/rdf_transform_{file}.log"
    message:
        "Generate RDF file to integrate in virtuoso"
    conda:
        pipeline_path + "/conda_env/python3.8.yaml"
    shell:
        pipeline_path + "/scripts/csv2ttl.py -i {input} -o {output} 2> {log}"

# (STEP6)
# Rule virtuoso_sparql
#  -> launch a local virtuoso server
#  -> load data in virtuoso server
#  -> request with python sparl wrapper
#  -> remove data
#  -> shutdown virtuoso
#

rule virtuoso_sparql:
    input:
        expand(output_dir + "/{file}.ttl", file=RDF_INPUT)
    output:
        output_dir + "/result.txt"
    log:
        log_dir + "/virtuoso_sparql.log"
    message:
        "Launch Virtuoso, load data and request"
    conda:
        pipeline_path + "/conda_env/virtuoso.yaml"
    shell:
        "virtuoso-t -c " + pipeline_path + "/virtuoso.ini 2> {log};"
        "sleep 30s;"
        "sh " + pipeline_path + "/scripts/load_data.sh " + output_dir + " 2>> {log};"
        "sleep 30s;"
        "echo 'request';"
        "python " + pipeline_path + "/scripts/sparql.py > {output} 2>> {log};"
        "sh " + pipeline_path + "/scripts/virtuoso-run-script.sh " + pipeline_path + "/scripts/shutdown.sql 2>> {log};"

rule clean_virtuoso_output:
    input:
        output_dir + "/result.txt"
    output:
        output_dir + "/TF_Gene_allProfiles.csv"
    log:
        log_dir + "/clean_virtuoso.log"
    message:
        "Clean virtuoso output"
    shell:
        "grep -v 'time =' {input} > {output} 2> {log}"

# (STEP7)
# Rule AllRel_to_ConsistRel
#  -> Filter all TF Gene relations according to consistency rules
#  -> Add a potential sign to relations, + for activation, - for inhibition
#

rule AllRel_to_ConsistRel:
    input:
        output_dir + "/TF_Gene_allProfiles.csv"
    output:
        output_dir + "/TF_Gene_ConsistRel.csv"
    log:
        log_dir + "/allRel_to_Consist.log"
    message:
        "Filtering and signing consistent TF Gene relations"
    conda:
        pipeline_path + "/conda_env/python3.8.yaml"
    shell:
        pipeline_path + "/scripts/tf_gene_all2consist.py -i {input} -o {output} 2> {log}" 
