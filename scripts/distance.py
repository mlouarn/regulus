#!/usr/bin/env python

import pandas as pd
import argparse

# Read command line and set args
parser = argparse.ArgumentParser(prog='distance', description='Calculate distances between regions and genes')
parser.add_argument('-g', '--gene_file',   nargs='?', help='Gene localisation file (bed)')
parser.add_argument('-r', '--region_file', nargs='?', help='Region localisation file (bed)')
parser.add_argument('-o', '--output_file', nargs='?', help='filename to output result')
args = parser.parse_args()

# If an argument is missing -> usage and exit
if not args.gene_file or not args.region_file or not args.output_file:
    parser.print_help()
    exit(1)

# Read Gene localisation file and select/rename the four first columns
print("Read Genes")
table_Gene = pd.read_csv(args.gene_file, header=None, delimiter='\t')
table_Gene = table_Gene.iloc[:,0:4]
table_Gene.columns = ['Chr', 'Start_Gene','End_Gene','Gene']

# Read Region localisation file and select/rename the four first columns
print("Read Regions")
table_Region = pd.read_csv(args.region_file, header=None, delimiter='\t')
table_Region = table_Region.iloc[:,0:4]
table_Region.columns = ['Chr', 'Start_Region','End_Region','Region']

print("Calculate Distances")

# Calculate distance between genes and regions (becareful df is a global variable)
def calc_dist(gene_start, gene_end, region_start, region_end):
	df['Distance'] = 0
	df.loc[region_end < gene_start, 'Distance'] = gene_start - region_end + 1
	df.loc[gene_end < region_start, 'Distance'] = region_start - gene_end - 1

# group genes and region by chromosome
gene_groups = table_Gene.groupby('Chr')
region_groups = table_Region.groupby('Chr')

# for each couple genes/region on the same chromosome:
#  - merge sub groups in df
#  - calculate distances on df
#  - filter distances <= 500000
df_groups = []
for r_chr, rg in region_groups:
	for g_chr, gg in gene_groups:
		if r_chr == g_chr:
			df = pd.merge(rg, gg, how='left', on='Chr')
			calc_dist(df['Start_Gene'].values,df['End_Gene'],df['Start_Region'],df['End_Region'])
			df_groups.append(df[df['Distance'] <= 500000])

# concatenate each dataframe 
full = pd.concat(df_groups)

# Write file
print("Write", args.output_file)
full.to_csv(args.output_file, index=True)