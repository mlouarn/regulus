# Regulus

Regulus is devoted to transcriptional regulatory network inference in settings with few samples and performs well with closely related cell types. 
Regulus creates signed TF-gene relations from TF binding sites (TFBS, provided), gene expression (RNA-seq) and regulatory regions activity (ATAC-seq, histone marks ChIP-seq) data. It requires normalized, cell-type averaged gene expressions and region activities, as well as genomic coordinates for genes and regions.

Regulus first creates patterns to group TF, genes and regions with similar expression / activity dynamics, computes distance relations between genes and regions and identifies inclusion of TFBS into regions. These relations between TF, genes and regions are then integrated into a RDF data structure model and SPARQL-queried to find, for each gene, the set of related regions (closer than 500 kb) containing TFBS of specific TF.
These TF-gene-region relations are then globally consistency-filtered by using the patterns values to identify the most probable TF-gene relations and to qualify them as inhibition or activation.

The end result is a table of signed TF-gene relations, indicating the involved region(s) and the respective patterns of all entities. These relations can further be merged into unique TF-gene relations if they involve different regions for the same TF and gene.

## Preliminaries

Regulus needs **snakemake** and **conda** available for setup.sh, it has been tested with 
* conda-4.7.12
* snakemake-5.20.1

Be careful with version of snakemake, Regulus uses the option `--conda-create-envs-only` which 
is `--create-envs-only` in older versions.

On a cluster, **snakemake and conda MUST be available** on every nodes.

## Install

Get the repository on gitlab.com, then go inside and launch setup.sh after activating **conda** and **snakemake** envs if needed:

```bash
git clone https://gitlab.com/teamDyliss/regulus.git
cd regulus
./setup.sh
```

The setup creates an executable file `regulus` which is used to launch the pipeline.
It also add `virtuoso.ini` file in the repository that corresponds to the default virtuoso server configuration (automatically installed by conda). You can modify this file to tune the virtuoso server (RAM usage, number of answers, etc).
We provide an optimized version of `virtuoso.ini` allowing to get up to 1e6 query answers per gene. You can find it under the `regulus` folder under the name `virtuoso_opt.ini`. If you want to use it (recommended), just remove the original `virtuoso.ini` file and rename `virtuoso_opt.ini` as `virtuoso.ini`.

## Regulus usage

```bash
./regulus --help
Regulus, a pipeline for regulatory circuits analysis
Usage: ./regulus -i|--input <arg> -o|--output <arg> [-c|--cluster <arg>] [-h|--help]
	-i, --input: Input directory absolute path (mandatory)
	-o, --output: Output directory absolute path (mandatory)
	-c, --cluster: Command to launch jobs on your cluster (sbatch, qsub...) (no default)
	-n, --dry-run: dry-run of regulus (off by default)
	-j, --jobs: Number of parallel jobs (default: '4')
	-h, --help: Prints help
	-v, --version: Prints version
```

As you can see, two arguments are mandatory: input and output directories. The pathes must be absolute from the root. Given these two arguments, Regulus creates the configuration files and launch the analysis as follows.

First, Regulus creates the ouput directory and a `workdir` directory inside. Then, in `workdir`, Regulus creates:
* `regulus_job.sh` the bash job that can be launched directly or through a cluster command 
* `smk_config.yaml` the snakemake configuration file containing variables used by `pipeline.smk`
* `smk_profile/config.yaml` the snakemake profile that defines the environment (cluster or not, number of parallel jobs, etc)

If `--dry-run` is set `on`, then regulus just creates the previous files and doesn't launch `regulus_job.sh`. This option let you modify parameters before launching the pipeline.

Otherwise, it launches `regulus_job.sh` given the cluster command (sbatch, qsub...) or directly with `bash` if `--cluster` option isn't used.

## Inputs

Four files are needed (the file names MUST be these):
* `gene_expression.csv`
* `region_read_density.csv`
* `gene_localisation.bed`
* `region_localisation.bed`

The localisation files (*.bed) are classical bed files, tab separated values, without headers, with the 5 standard columns : chromosome, start, stop, name, strand. It is supposed that start position is always less than end position.

Please note that since this is a `snakemake` pipeline, you can independently compute any intermediary file and put it in the `output` folder to skip some step (this is also useful when you want to restart your pipeline or only change part of the input files).
Just be careful to use the **correct file names** when doing so, they can be found as input or output of each rule in the `pipeline.smk` file.

## Very important note about data !!
`gene_expression.csv` must have a header where the first column name is `Gene`, other columns are not constrainted.
`region_read_density.csv` must have a header where the first column name is `Region`, other columns are not constrainted. In these files, each column than the first is related to a population and values are floats.

For now, we only provide a version in hg19 coordinates. All your genomic files must be in hg19 and chromosome sorted to be compliant with bedtools requirements and the hg19.genome2 reference file provided. You can use `sort -k1,1V -k2,2n` on your bed files to sort them correctly.

## Outputs

The following files are generated by _Regulus_ and will ber found in the output/ folder:
* `gene_pattern_count.csv`: number of genes for each expression pattern.
* `gene_pattern.csv`, `gene_pattern.ttl`: expression patterns for each gene in csv and ttl (RDF compliant) formats.
* `gene_region_distance.csv`, `gene_region_distance_filtered.csv`, `gene_region_distance_filtered.ttl`: computed distances between genes and regions, as a complete file, reduced to the minimal distance and in ttl format.
* `region_pattern.csv`, `region_pattern.ttl`: activity patterns for each region in csv and ttl formats.
* `result.txt`: raw SPARQL query results, with computing time for each gene.
* **`TF_Gene_allProfiles.csv`**: all TF-gene relations obtained after running the SPARQL query.
* **`TF_Gene_ConsistRel.csv`**: Consistency filtered TF-gene relations (filtering of the previous file).
* `tf_pattern.csv`, `tf_pattern.ttl`: expresion patterns for each TF in csv and ttl.
* `tf_region_intersect.csv`, `tf_region_intersect_filtered.csv`, `tf_region_intersect_filtered.ttl`: intersection between TF binding sites and regulatory regions, as a complete dataset, reduced to single occurence of one TFBS in a given region and in ttl format.
